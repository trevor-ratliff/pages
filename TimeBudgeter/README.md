# Time Budgeter

This application is used for budgeting your time in a day.  It is intended to 
help you work on the more useful items so they don't be come emergencies.  In a 
web application it will be limited on the notifications it can provide for 
keeping you on task.


## Goals

The main goal of this project is to re-create an application I had made a long 
time ago.  I'd like to improve some aspects of the application, and test out 
some new ideas.


## Requirements

1. Track a list of activities
	1. an activity will have the following properties
		- description
		- due date and time
		- importance
		- title
1. The activites will sort themselves based on urgency and importance
	- urgency: the need to do something with a deadline
		1. nonurgent items will move from nonurgent to urgent within a 
			configurable amount of time of the due date; defaults to 1 hour
		1. urgent items will move to ugent and important within a configurable 
			amount of time of the due date; defaults to 1/2 hour
	- importance: the weight of an activity compared to others
		1. unimportant items will move from unimportant to important within a 
			configurable amount of time from the due date/time; defaults to 4 
			hours
		1. important and nonurgent items will move from nonurgent to urgent 
			according to urgent rules
1. Track reminders
	1. reminders are repeating activities that won't show until a configurable 
		amoun of time from the reminder time
		1. reminder schedule to be set with cron like syntax
		1. use cron library?
1. Track To Dos ???
1. Items will be stored in browser memory via a storage mechanism (local storage
	or indexdb)
	- structure:
```
{
	"activities": [
		{
			"id": "",
			"title": "",
			"description": "",
			"improntance": "",
			"due": ""
		}
	],
	"reminders": [
		{
			"id": "",
			"title": "",
			"description": "",
			"improntance": "",
			"due": "",
			"repeat": ""
		}
	]
}
```
