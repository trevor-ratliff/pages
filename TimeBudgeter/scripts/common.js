//====
/// @file common.js
/// @brief This is an EcmaScript module file to share common functions between 
///	a main page and a web worker
/// @author Trevor Ratliff
/// @date 2023-04-04
/// @verbatim
//  
//  Definitions:
//	timeBudgeter - object holding main application logic
//	sortItmes() - a function to sort actions and reminders
//	common - a class for importing in other code ... not working
//  
/// @History:  Date  |  Programmer  |  Contact  |  Description  |
///	2023-04-04  |  Trevor Ratliff  |  trevor.w.ratliff@gmail.com  |  file 
///		creation in order to try out a EcmaScript module and hold sort code
///	2023-04-20  |  Trevor Ratliff  |  trevor.w.ratliff@gmail.com  |  trying to 
///		get the class to export/import ... still not working
/// @endverbatim
//====

var timeBudgeter = {};

function sortItems(a, b) {
	const aUrgImp = (a.urgence === 'urgent' ? 10 : 0) 
		+ (a.importance === 'important' ? 1 : 0);
	const bUrgImp = (b.urgence === 'urgent' ? 10 : 0) 
		+ (b.importance === 'important' ? 1 : 0);
	const aDate = Date.parse(a.due);
	const bDate = Date.parse(b.due);

	// default to no change in sort order
	let ret = 0;

	// if the importance and urgency is the same sort on due date
	if (aUrgImp === bUrgImp) {
		// if the due dates are equal, but the types aren't sort the reminders first
		if (aDate == bDate && a.type != b.type) {
			ret = a.type === 'activity' ? -1 : 1;
		}

		// if  a.due is later move it right
		if (aDate > bDate) ret = 1;

		// if a.due is earlier move it left
		if (aDate < bDate) ret = -1;
	} else {
		// sort urgent things over important things,  move it left
		ret = aUrgImp > bUrgImp ? -1 : 1;
	}
	return ret; 
}


class common {
	constructor() {
	}
	
	// do stuff
	//let tb = timeBudgeter || {};

	//----
	/// @fn timeBudgeter.SortItems()
	/// @brief sorts the items based on due date, priority
	//----
	SortItems = (a, b) => { 
		// tb = timeBudgeter || {};
		const aUrgImp = (a.urgence === 'urgent' ? 10 : 0) 
			+ (a.importance === 'important' ? 1 : 0);
		const bUrgImp = (b.urgence === 'urgent' ? 10 : 0) 
			+ (b.importance === 'important' ? 1 : 0);
		const aDate = Date.parse(a.due);
		const bDate = Date.parse(b.due);
		// if (tb.logLevel <= 1) console.log('sorting items', aUrgImp, aDate, a, bUrgImp, bDate, b);

		// default to no change in sort order
		let ret = 0;

		// if the importance and urgency is the same sort on due date
		if (aUrgImp === bUrgImp) {
			// if the due dates are equal, but the types aren't sort the reminders first
			if (aDate == bDate && a.type != b.type) {
				ret = a.type === 'activity' ? -1 : 1;
			}

			// if  a.due is later move it right
			if (aDate > bDate) ret = 1;

			// if a.due is earlier move it left
			if (aDate < bDate) ret = -1;
		} else {
			// sort urgent things over important things, move it left
			ret = aUrgImp > bUrgImp ? -1 : 1;
		}
		return ret; 
	};
};

export { sortItems, common };