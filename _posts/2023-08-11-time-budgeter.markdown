---
layout: post
title:  "TimeBudgeter"
date:   2023-08-11 18:45:11 -0500
categories: software
---
# TimeBudgeter

The TimeBudgeter application started a long time ago as an .net 1.2 application. 
I used it as an excuse to play around with the new framework. 
I had recently taken a class about the "7 Habits of Highly Effective People" and 
wanted to apply some of that knowledge to my day to day life.
